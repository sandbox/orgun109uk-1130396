<?php
/**
 * @file
 * A form to list all replication reports.
 */

/**
 * Form builder for the replication reports list form.
 *
 * @ingroup forms
 */
function replication_reports_list(&$form_state) {
  $path = drupal_get_path("module", "replication_reports");
  drupal_add_css("{$path}/assets/css/replication-reports-admin.css");

  $form = array();
  return $form;
}

/**
 * Returns HTML for the replication reports list form.
 *
 * @ingroup themeable
 */
function theme_replication_reports_list($form) {
  $header = array(
    array("data" => t("Status"), "field" => "r.status"),
    array("data" => t("Report start"), "field" => "r.report_start", "sort" => "asc"),
    array("data" => t("Report end"), "field" => "r.report_end"),
    array("data" => t("Job name"), "field" => "j.name"),
    t("Operations")
  );

  $limit = 25;
  $dbq = pager_query("SELECT r.status, r.report_start, r.report_end, j.name FROM {replication_reports} AS r LEFT JOIN {replication_jobs} AS j ON r.jid = j.jid" . tablesort_sql($header), $limit);

  $rows = array();
  $is_odd = TRUE;
  while ($dbr = db_fetch_object($dbq)) {
    $rows[] = array(
      "data" => array(
        array("data" => "&nbsp;", "class" => "report-status report-status-{$dbr->status}"),
        array("data" => drupal_date($dbr->report_start), "class" => "report-start"),
        array("data" => drupal_date($dbr->report_end), "class" => "report-end"),
        array("data" => $dbr->name, "class" => "report-job-name"),
        array(
          "data" =>
            l(t("view"), "admin/build/replication/reports/view/{$cid}") . " | " .
            l(t("delete"), "admin/build/replication/connections/delete/{$cid}"),
          "class" => "report-ops"
        )
      ),
      "class" => "report-details report-" . ($is_odd ? "odd" : "even")
    );
    $is_odd = !$is_odd;
  }

  if (empty($rows)) {
    $rows[] = array(
      array("data" => t("There are no reports available."))
    );
  }

  $output = "";
  if (!empty($rows)) {
    $output .= theme("table", $header, $rows, array(), NULL);
    $pager = array(t("first"), t("&laquo; previous"), "", t("next &raquo;"), t("last"));
    $output .= theme("pager", $pager, $limit);
  }
  $output .= drupal_render($form);
  return $output;
}

