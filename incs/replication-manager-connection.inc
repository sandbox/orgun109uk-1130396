<?php
/*******************************************************************************
 * @file
 * This file contains all the utility functions for connections.
 */

/*******************************************************************************
 * Connections.
 */
function replication_manager_unmask_connection(&$connection) {
  if (is_numeric($connection)) $connection = replication_manager_load_connection($connection);
  if (!user_access("view unmasked passwords")) return FALSE;
  if ($connection->unmasked === TRUE) return TRUE;

  $connection->unmasked = TRUE;
  $connection->password = replication_manager_crypt($connection->password, REPLICATION_MANAGER_CONNECTION_CRYPTKEY . $connection->connection_name);
  return TRUE;
}

function replication_manager_check_connection(&$connection) {
  if (empty($connection)) return FALSE;
  if (is_numeric($connection)) $connection = replication_manager_load_connection($connection, TRUE);
  if (!is_object($connection)) return FALSE;
  $connection->status = REPLICATION_MANAGER_CONNECTION_STATUS_ERROR;

  if (!(empty($connection->host) || empty($connection->username))) {
    replication_manager_unmask_connection($connection);
    $dbtype = replication_manager_dbtypes($connection->dbtype);
    if (!empty($dbtype)) {
      $dbo = replication_manager_dbtype_object($connection, $dbtype);
      if ($dbo->check())
        $connection->status = REPLICATION_MANAGER_CONNECTION_STATUS_OK;
    }
    else {
      $connection->status_response = t("Failed to obtain the database type object.");
    }
  }
  else {
    $connection->status_response = t("The connection is missing the host details and/or a username.");
  }
  return TRUE;
}

function replication_manager_load_connection($cid = NULL, $unmask_password = FALSE, $check_status = FALSE) {
  if (is_numeric($cid)) {
    $dbq = db_query("SELECT * FROM {replication_connections} WHERE cid = %d", $cid);
  }
  else {
    $dbq = db_query("SELECT * FROM {replication_connections}");
  }

  $connections = array();
  while ($dbr = db_fetch_object($dbq)) {
    if ($unmask_password) replication_manager_unmask_connection($dbr);
    if ($check_status) replication_manager_check_connection($dbr);
    $connections[$dbr->cid] = $dbr;
  }

  if (is_numeric($cid)) return $connections[$cid];
  return $connections;
}

function replication_manager_save_connection(&$connection) {
  if (is_array($connection)) {
    $r = array();
    foreach ($connection as &$conn)
      $r[] = replication_manager_save_connection($conn);
    return $r;
  }

  if (empty($connection->cid)) {
    global $user;
    $connection->created = time();
    $connection->created_uid = $user->uid;
  }

  $connection->modified = time();
  $connection->modified_uid = $user->uid;
  return drupal_write_record("replication_connections", $connection, !empty($connection->cid) ? "cid" : array());
}

function replication_manager_delete_connection(&$connection) {
  if (is_array($connection)) {
    $r = array();
    foreach ($connection as &$conn)
      $r[] = replication_manager_delete_connection(&$conn);
    return $r;
  }
  return db_query("DELETE FROM {replication_connections} WHERE cid = %d", $connection->cid);
}

