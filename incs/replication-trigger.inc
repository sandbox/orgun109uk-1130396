<?php
/*******************************************************************************
 * @file
 * This file contains the replication trigger.
 */

/*******************************************************************************
 * Triggers a given replication job.
 *
 * @param $jid
 *   The given JID of a job to trigger.
 */
function replication_manager_trigger($jid = NULL) {
  if (!is_numeric($jid)) {
    $req_jid = $jid;
    $jid = NULL;
  }

  if ($jid == NULL) {
    watchdog("Replication", "Attempted to trigger an invalid replication job" . (!empty($req_jid) ? " '{$old_jid}'" : "") . ".", array(), WATCHDOG_ERROR);
    return drupal_not_found();
  }

  $job = replication_manager_load_job($jid, TRUE);
  if (empty($job)) {
    watchdog("Replication", "Attempted to trigger an unknown replication job ({$jid}).", array(), WATCHDOG_ERROR);
    return drupal_not_found();
  }
  
  $context = replication_manager_run_job($job, REPLICATION_MANAGER_JOB_RUNMODE_TRIGGER);
  if (!$context->success) {
    if (!empty($context->redirect))
      drupal_goto($context->redirect);
    else
      drupal_not_found();
  }
  else {
    drupal_goto(!empty($context->redirect) ? $context->redirect : "");
  }
}

