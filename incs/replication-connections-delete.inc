<?php
/**
 * @file
 * A form to confirm the deletion of the connection.
 */

/**
 * Form builder for the delete connection confirmation form.
 *
 * @param $cid
 *   The connections CID to delete.
 *
 * @see replication_manager_connections_delete_submit()
 * @ingroup forms
 */
function replication_manager_connections_delete(&$form_state, $cid = NULL) {
  $connection = NULL;
  if (is_numeric($cid)) $connection = replication_manager_load_connection($cid);

  $form = array();
  if (empty($connection)) {
    $form["info"] = array(
      "#prefix" => "<p>",
      "#value" => t("The provided connection id <b>!cid</b> is invalid or failed to load a valid connection.", array("!cid" => $cid)),
      "#suffix" => "</p>"
    );
    return $form;
  }
  else {
    $form["#connection"] = $connection;
    return confirm_form($form,
      t("Are you sure you want to delete the connection %name?", array("%name" => $connection->connection_name)),
      "admin/build/replication/connections/list",
      t("This action can <b>not</b> be undone."),
      t("Delete connection"),
      t("Cancel"),
      "replication_manager_connections_delete"
    );
  }
}

/**
 * Form submission handler for replication_manager_connections_delete().
 *
 * @see replication_manager_connections_delete()
 */
function replication_manager_connections_delete_submit($form, &$form_state) {
  $connection = $form["#connection"];
  if (empty($connection) || empty($connection->cid)) {
    drupal_set_message(t("Failed to delete connection, as a valid connection was not provided."), "warning");
  }
  else {
    if (replication_manager_delete_connection($connection) !== FALSE) {
      drupal_set_message(t("Successfully deleted the connection %name.", array("%name" => $connection->connection_name)));
    }
    else {
      drupal_set_message(t("Failed to deleted the connection %name.", array("%name" => $connection->connection_name)));
    }
  }
  $form_state["redirect"] = "admin/build/replication/connections/list";
}

