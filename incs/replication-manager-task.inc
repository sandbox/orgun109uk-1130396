<?php
/*******************************************************************************
 * @file
 * This file contains all the utility functions for tasks.
 */

/*******************************************************************************
 * Tasks.
 */
function replication_manager_load_task($tid = NULL, $jid = NULL) {
  if (is_numeric($tid)) {
    $dbq = db_query("SELECT * FROM {replication_tasks} WHERE tid = %d", $tid);
  }
  elseif (is_numeric($jid)) {
    $dbq = db_query("SELECT * FROM {replication_tasks} WHERE jid = %d", $jid);
  }
  else {
    $dbq = db_query("SELECT * FROM {replication_tasks}");
  }

  $tasks = array();
  while ($dbr = db_fetch_object($dbq)) {
    if (is_string($dbr->task_settings)) $dbr->task_settings = unserialize($dbr->task_settings);
    $tasks[$dbr->tid] = $dbr;
  }

  if (is_numeric($tid)) return $tasks[$tid];
  return $tasks;
}

function replication_manager_save_task(&$task) {
  if (is_array($task)) {
    $r = array();
    foreach ($task as &$t)
      $r[] = replication_manager_save_task($t);
    return $r;
  }

  return drupal_write_record("replication_tasks", $task, !empty($task->tid) ? "tid" : array());
}

function replication_manager_delete_task(&$task) {
  if (is_array($task)) {
    $r = array();
    foreach ($task as &$t)
      $r[] = replication_manager_delete_task(&$t);
    return $r;
  }

  return db_query("DELETE FROM {replication_tasks} WHERE tid = %d", $task->tid);
}

