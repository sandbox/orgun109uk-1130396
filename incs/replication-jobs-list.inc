<?php
/**
 * @file
 * A form to list all jobs that have been setup.
 */

/**
 * Form builder for the jobs list form.
 *
 * @ingroup forms
 */
function replication_manager_jobs_list(&$form_state) {
  $path = drupal_get_path("module", "replication_manager");
  drupal_add_css("{$path}/assets/css/replication_manager-admin.css");
  $form = array();
  return $form;
}

/**
 * Returns HTML for the connections list form.
 *
 * @ingroup themeable
 */
function theme_replication_manager_jobs_list($form) {
  $header = array(
    t("Status"),
    t("Name"),
    t("Job type"),
    t("Created"),
    t("Modified"),
    t("Last run"),
    t("Total runs (Successes / Fails)"),
    t("Operations")
  );

  $created_by = array();

  $jobs = replication_manager_load_job(NULL, FALSE, variable_get("replication_manager_jobslist_autostatus", FALSE));
  $rows = array();
  $is_odd = TRUE;
  foreach ($jobs as $jid => $job) {
    $job_type = replication_manager_jobtypes($job->job_type);
    if (empty($job->status)) $job->status = REPLICATION_MANAGER_JOB_STATUS_UNKNOWN;
    if (empty($created_by[$job->created_uid])) {
      $created_by[$job->created_uid] = db_result(db_query("SELECT name FROM {users} WHERE uid = %d", $job->created_uid));
    }

    if (empty($created_by[$job->modified_uid])) {
      $created_by[$job->modified_uid] = db_result(db_query("SELECT name FROM {users} WHERE uid = %d", $job->modified_uid));
    }
    
    if (empty($created_by[$job->last_run_uid])) {
      $created_by[$job->last_run_uid] = db_result(db_query("SELECT name FROM {users} WHERE uid = %d", $job->last_run_uid));
    }

    $rows[] = array(
      "data" => array(
        array("data" => "&nbsp;", "class" => "job-status job-status-{$job->status}", "rowspan" => 2),
        array("data" => $job->job_name, "class" => "job-name", "rowspan" => 2),
        array("data" => !empty($job_type) ? $job_type["name"] : t("Unknown job type"), "class" => "job-jobtype"),
        array("data" => format_date($job->created) . " " . t("by") . " " . (!empty($created_by[$job->created_uid]) ? l($created_by[$job->created_uid], "user/{$job->created_uid}") : $job->created_uid), "class" => "job-created"),
        array("data" => format_date($job->modified) ." " . t("by") . " " . (!empty($created_by[$job->modified_uid]) ? l($created_by[$job->modified_uid], "user/{$job->modified_uid}") : $job->modified_uid), "class" => "job-modified"),
        array("data" => format_date($job->last_run) . " " . t("by") . " " . (!empty($created_by[$job->last_run_uid]) ? l($created_by[$job->last_run_uid], "user/{$job->last_run_uid}") : $job->last_run_uid), "class" => "job-lastrun"),
        array("data" => "{$job->runs} (<span style=\"color:green;\">{$job->successes}</span> / <span style=\"color:red;\">{$job->fails}</span>)", "class" => "job-runs"),
        array(
          "data" =>
            ($job_type["triggable"] ? l(t("run"), "replication/{$jid}") : t("run")) . " | " .
            l(t("check"), "admin/build/replication/jobs/check/{$jid}") . " | " .
            l(t("edit"), "admin/build/replication/jobs/edit/{$jid}") . " | " .
            l(t("delete"), "admin/build/replication/jobs/delete/{$jid}"),
          "class" => "job-ops"
        )
      ),
      "class" => "job-details job-" . ($is_odd ? "odd" : "even")
    );

    $rows[] = array(
      "data" => array(
        array("data" => $job->job_description, "colspan" => 8)
      ),
      "class" => "job-description job-" . ($is_odd ? "odd" : "even")
    );

    $is_odd = !$is_odd;
  }

  if (empty($rows)) {
    $rows[] = array(
      array("data" => t("There are no jobs available. !add a job.", array("!add" => l(t("Add"), "admin/build/replication/jobs/add"))))
    );
  }

  $output = "";
  if (!empty($rows)) {
    $output .= theme("table", $header, $rows, array(), NULL);
  }
  $output .= drupal_render($form);
  return $output;
}

