<?php
/**
 * @file
 * A form to list all connections that have been setup.
 */

/**
 * Form builder for the connections list form.
 *
 * @ingroup forms
 */
function replication_manager_connections_list(&$form_state) {
  $path = drupal_get_path("module", "replication_manager");
  drupal_add_css("{$path}/assets/css/replication_manager-admin.css");

  $form = array();
  return $form;
}

/**
 * Returns HTML for the connections list form.
 *
 * @ingroup themeable
 */
function theme_replication_manager_connections_list($form) {
  $header = array(
    t("Status"),
    t("Name"),
    t("Database type"),
    t("Host"),
    t("Database"),
    t("Prefix"),
    t("Username"),
    t("Password"),
    t("Created"),
    t("Modified"),
    t("Operations")
  );

  $created_by = array();
  $connections = replication_manager_load_connection(NULL, !variable_get("replication_manager_maskpasswords", TRUE), variable_get("replication_manager_connectionslist_autostatus", FALSE));

  $rows = array();
  $is_odd = TRUE;
  $maskpassword = variable_get("replication_manager_maskpasswords", TRUE);
  foreach ($connections as $cid => $connection) {
    $dbtype = replication_manager_dbtypes($connection->dbtype);

    if (empty($created_by[$connection->created_uid])) {
      $created_by[$connection->created_uid] = db_result(db_query("SELECT name FROM {users} WHERE uid = %d", $connection->created_uid));
    }

    if (empty($created_by[$connection->modified_uid])) {
      $created_by[$connection->modified_uid] = db_result(db_query("SELECT name FROM {users} WHERE uid = %d", $connection->modified_uid));
    }

    $rows[] = array(
      "data" => array(
        array("data" => "&nbsp;", "class" => "connection-status connection-status-{$connection->status}", "rowspan" => 2),
        array("data" => $connection->connection_name, "class" => "connection-name", "rowspan" => 2),
        array("data" => !empty($dbtype) ? $dbtype["name"] : t("Unknown database type"), "class" => "connection-dbtype"),
        array("data" => "{$connection->host}:{$connection->port}", "class" => "connection-host"),
        array("data" => $connection->database_name, "class" => "connection-database-name"),
        array("data" => $connection->table_prefix, "class" => "connection-table-prefix"),
        array("data" => $connection->username, "class" => "connection-uname"),
        array("data" => $maskpassword ? replication_manager_maskpassword($connection->password) : $connection->password, "class" => "connection-pword"),
        array("data" => format_date($connection->created) . " " . t("by") . " " . (!empty($created_by[$connection->created_uid]) ? l($created_by[$connection->created_uid], "user/{$connection->created_uid}") : $connection->created_uid), "class" => "connection-created"),
        array("data" => format_date($connection->modified) . " " . t("by") . " " . (!empty($created_by[$connection->modified_uid]) ? l($created_by[$connection->modified_uid], "user/{$connection->modified_uid}") : $connection->modified_uid), "class" => "connection-modified"),
        array(
          "data" =>
            l(t("check"), "admin/build/replication/connections/check/{$cid}") . " | " .
            l(t("edit"), "admin/build/replication/connections/edit/{$cid}") . " | " .
            l(t("delete"), "admin/build/replication/connections/delete/{$cid}"),
          "class" => "connection-ops"
        )
      ),
      "class" => "connection-details connection-" . ($is_odd ? "odd" : "even")
    );

    $rows[] = array(
      "data" => array(
        array("data" => $connection->connection_description, "colspan" => 11)
      ),
      "class" => "connection-description connection-" . ($is_odd ? "odd" : "even")
    );
    $is_odd = !$is_odd;
  }

  if (empty($rows)) {
    $rows[] = array(
      array("data" => t("There are no connections available. !add a connection.", array("!add" => l(t("Add"), "admin/build/replication/connections/add"))))
    );
  }

  $output = "";
  if (!empty($rows)) {
    $output .= theme("table", $header, $rows, array(), NULL);
  }
  $output .= drupal_render($form);
  return $output;
}

