<?php
/**
 * @file
 * A form to provide a connection test..
 */

/**
 * Form builder for the connection check form.
 *
 * @param $cid
 *   The connections CID to check.
 *
 * @ingroup forms
 */
function replication_manager_connections_check(&$form_state, $cid = NULL) {
  $connection = NULL;
  $maskpassword = variable_get("replication_manager_maskpasswords", TRUE);
  if (is_numeric($cid)) $connection = replication_manager_load_connection($cid, !$maskpassword);

  $form = array();
  if (empty($connection)) {
    $form["info"] = array(
      "#prefix" => "<p>",
      "#value" => t("The provided connection id <b>!cid</b> is invalid or failed to load a valid connection.", array("!cid" => $cid)),
      "#suffix" => "</p>"
    );
  }
  else {
    $dbtype = replication_manager_dbtypes($connection->dbtype);
    if (empty($dbtype)) {
      $form["info"] = array(
        "#prefix" => "<p>",
        "#value" => t("The connection <b>%name</b> does not have a valid database connection type. Check in the connections !settings.", array("%name" => $connection->connection_name, "!settings" => l(t("settings"), "admin/build/replication/connections/edit/{$connection->cid}"))),
        "#suffix" => "</p>"
      );
    }
    else {
      $form["connection"] = array(
        "#type" => "fieldset",
        "#title" => t("Connection details"),
        "#collapsible" => TRUE,
        "connection_name" => array(
          "#prefix" => "<div>",
          "#value" => t("<b>Name: </b>!name", array("!name" => $connection->connection_name)),
          "#suffix" => "</div>"
        ),
        "connection_description" => array(
          "#prefix" => "<div>",
          "#value" => t("<b>Description: </b>!description", array("!description" => $connection->connection_description)),
          "#suffix" => "</div>"
        ),
        "connection_host" => array(
          "#prefix" => "<div>",
          "#value" => t("<b>Host: </b>!host", array("!host" => "{$connection->host}:{$connection->port}")),
          "#suffix" => "</div>"
        ),
        "connection_username" => array(
          "#prefix" => "<div>",
          "#value" => t("<b>Username: </b>!username", array("!username" => $connection->username)),
          "#suffix" => "</div>"
        ),
        "connection_password" => array(
          "#prefix" => "<div>",
          "#value" => t("<b>Password: </b>!password", array("!password" => $maskpassword ? replication_manager_maskpassword($connection->password) : $connection->password)),
          "#suffix" => "</div>"
        )
      );

      replication_manager_check_connection($connection);
      $form["dbtype"] = array(
        "#type" => "fieldset",
        "#title" => t("Database type"),
        "#collapsible" => TRUE,
        "dbtype_name" => array(
          "#prefix" => "<div>",
          "#value" => t("<b>Name: </b>!name", array("!name" => $dbtype["name"])),
          "#suffix" => "</div>"
        ),
        "dbtype_status" => array(
          "#prefix" => "<div>",
          "#value" => t("<b>Status: </b>!status", array("!status" => $connection->status == REPLICATION_MANAGER_CONNECTION_STATUS_OK ? "<span style=\"color:green;\">Successful</span>" : "<span style=\"color:red;\">Failed</span>")),
          "#suffix" => "</div>"
        ),
        "dbtype_msg" => array(
          "#prefix" => "<div>",
          "#value" => t("<b>Repsonse: </b><br />!response", array("!response" => !empty($connection->status_response) ? $connection->status_response : "-")),
          "#suffix" => "</div>"
        )
      );

      $dbo = replication_manager_dbtype_object($connection, $dbtype);
      $tables = $dbo->get_tables(TRUE);
      if (is_array($tables)) {
        $form["tables"] = array(
          "#type" => "fieldset",
          "#title" => t("Tables information (!numtables tables)", array("!numtables" => count($tables))),
          "#collapsible" => TRUE,
          "#collapsed" => TRUE
        );

        $header = array(
          t("Field name"),
          t("Type"),
          t("Null"),
          t("Key"),
          t("Default"),
          t("Extra")
        );

        foreach ($tables as $table => $fields) {
          if (is_array($fields)) {
            $rows = array();

            $is_odd = TRUE;
            foreach ($fields as $field) {
              $rows[] = array(
                "data" => array(
                  array("data" => $field["name"], "class" => "replication-dbtype-name"),
                  array("data" => $field["type"], "class" => "replication-dbtype-type"),
                  array("data" => $field["null"], "class" => "replication-dbtype-null"),
                  array("data" => $field["key"], "class" => "replication-dbtype-key"),
                  array("data" => $field["default"], "class" => "replication-dbtype-default"),
                  array("data" => $field["extra"], "class" => "replication-dbtype-extra"),
                ),
                "class" => "replication-dbtype-field " . ($is_odd ? "odd" : "even")
              );
              $is_odd = !$is_odd;
            }
            $value = theme("table", $header, $rows);
          }
          else {
            $value = t("Failed to retrieve field information.<br /><b>Error: </b>!error", array("!error" => $connection->status_response));
          }

          $form["tables"][$table] = array(
            "#type" => "fieldset",
            "#title" => t("Table: !table", array("!table" => $table)),
            "#collapsible" => TRUE,
            "#collapsed" => TRUE,
            "#value" => $value
          );
        }
      }
      else {
        $form["tables"] = array(
          "#prefix" => "<p>",
          "#value" => t("Failed to retrieve a list of the database's tables.<br /><b>Error: </b>!error", array("!error" => $connection->status_response)),
          "#prefix" => "</p>"
        );
      }
    }
  }

  return confirm_form($form,
    "",
    "admin/build/replication/connections/list",
    "",
    t("Check again"),
    t("Back to connections list"),
    "replication_manager_connections_check"
  );
}

