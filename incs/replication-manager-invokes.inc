<?php
/*******************************************************************************
 * @file
 * This file contains all the invokes.
 */

/*******************************************************************************
 * Invokes.
 */
function _replication_manager_dbtypes($module = NULL, $op = "info", $settings = NULL) {
  $hook = "replication_manager_dbtypes";
  $rtn = FALSE;

  if (!empty($module)) {
    $function = "{$module}_{$hook}";
    if (function_exists($function)) {
      return $function($op, $settings);
    }
    else {
      return NULL;
    }
  }
  else {
    $rtn = array();
    foreach (module_implements($hook) as $module) {
      $function = "{$module}_{$hook}";
      $r = $function($op, $settings);

      if ($op == "info") {
        if (!empty($r) && is_array($r)) {
          foreach ($r as &$_r) $_r["module"] = $module;
          $rtn = array_merge($rtn, $r);
        }
      }
    }
    return $rtn;
  }
}

function _replication_manager_jobtypes($module = NULL, $op = "info", &$job_type = NULL, &$context = NULL) {
  $hook = "replication_manager_jobtypes";
  $rtn = FALSE;

  if (!empty($module)) {
    $function = "{$module}_{$hook}";
    if (function_exists($function)) {
      return $function($op, $job_type, $context);
    }
    else {
      return NULL;
    }
  }
  else {
    $rtn = array();
    foreach (module_implements($hook) as $_module) {
      $function = "{$_module}_{$hook}";

      if ($op == "info") {
        $r = $function($op);

        if (!empty($r) && is_array($r)) {
          foreach ($r as &$_r) $_r["module"] = $_module;
          $rtn = array_merge($rtn, $r);
        }
      }
      elseif ($op == "run") {
        return $function($op, $job_type, $context);
      }
    }
    if ($op == "info")
      return $rtn;
  }
}

function _replication_manager_process($op = "preprocess", &$job, &$context) {
  $hook = "replication_manager_process";
  foreach (module_implements($hook) as $_module) {
    $function = "{$_module}_{$hook}";
    if (function_exists($function))
      $function($op, $job, $context);
  }
}

