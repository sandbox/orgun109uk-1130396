<?php
/**
 * @file
 * The base replication database type class. Additional database types must
 * inherit from this class.
 */

class ReplicationDBType {
  protected $connection = NULL;
  protected $settings = NULL;
  protected $status_response = NULL;

  function __construct($settings) {
    $this->settings = $settings;
  }

  public function get_settings() {
    return $this->settings;
  }

  public function set_settings($settings) {
    $this->settings = $settings;
  }

  public function get_status_response() {
    return $this->status_response;
  }

  public function check() {
    $this->status_response = NULL;
    if ($this->connect()) {
      return $this->disconnect();
    }
    return FALSE;
  }

  public function connect() {
    $this->status_response = NULL;
    if (empty($this->settings)) {
      $this->status_response = t("Missing the connection settings.");
      return FALSE;
    }
    $this->connection = $this->_connect();
    if (!$this->connection) {
      $this->status_response = t("Failed to connect: !error", array("!error" => $this->_error()));
      return FALSE;
    }
    return TRUE;
  }

  public function disconnect() {
    $this->status_response = NULL;
    if (empty($this->connection)) {
      $this->status_response = t("Can not close a connection that has not been made yet.");
      return FALSE;
    }
    $this->_disconnect();
    $this->connection = NULL;
    return TRUE;
  }

  public function query($query) {
    $this->status_response = NULL;
    if (empty($this->connection)) $this->connect();
    if (empty($this->connection)) return FALSE;

    $dbq = $this->_query($query);
    if (!$dbq) {
      $this->status_response = t("Could not run query: !error", array("!error" => $this->_error()));
      return FALSE;
    }

    return $dbq;
  }

  public function fetch_array($dbq) {
    $this->status_response = NULL;
    if (empty($this->connection)) {
      $this->status_response = t("The connection has not yet been established.");
      return FALSE;
    }

    if (empty($dbq)) {
      $this->status_response = t("A query has not been given.");
      return FALSE;
    }

    return $this->_fetch_array($dbq);
  }

  public function fetch_object($dbq) {
    $this->status_response = NULL;
    if (empty($this->connection)) {
      $this->status_response = t("The connection has not yet been established.");
      return FALSE;
    }

    if (empty($dbq)) {
      $this->status_response = t("A query has not been given.");
      return FALSE;
    }
    return $this->_fetch_object($dbq);
  }

  public function count($dbq) {
    $this->status_response = NULL;
    if (empty($this->connection)) {
      $this->status_response = t("The connection has not yet been established.");
      return FALSE;
    }

    if (empty($query_ref)) {
      $this->status_response = t("A query has not been given.");
      return FALSE;
    }
    return $this->_count($dbq);
  }

  public function truncate($table) {
    $this->status_response = NULL;
    if (empty($this->connection)) $this->connect();
    if (empty($this->connection)) return FALSE;
    $table = check_plain($table);
    return $this->_truncate($table);
  }

  public function show_create_table($table, $dst_table) {
    $this->status_response = NULL;
    if (empty($this->connection)) $this->connect();
    if (empty($this->connection)) return FALSE;
    $table = check_plain($table);
    return $this->_show_create_table($table, $dst_table);
  }

  public function drop_table($table) {
    $this->status_response = NULL;
    if (empty($this->connection)) $this->connect();
    if (empty($this->connection)) return FALSE;
    $table = check_plain($table);
    return $this->_drop_table($table);
  }

  public function drop_tables() {
    $this->status_response = NULL;
    $tables = $this->get_tables();
    foreach ($tables as $table) $this->drop_table($table);
  }

  public function get_tables($inc_fields = FALSE) {
    $this->status_response = NULL;
    if (empty($this->connection)) $this->connect();
    if (empty($this->connection)) return FALSE;

    $tables = array();
    $dbq = $this->_get_tables();
    if (!$dbq) return FALSE;
    while ($dbr = $this->_fetch_array($dbq)) {
      $tables[] = $dbr[0];
    }

    if ($inc_fields) {
      $old = $tables;
      $tables = array();
      foreach ($old as $table) {
        $tables[$table] = $this->get_fields($table);
      }
    }
    return $tables;
  }

  public function get_fields($table) {
    $this->status_response = NULL;
    if (empty($this->connection)) $this->connect();
    if (empty($this->connection)) return FALSE;
    $table = check_plain($table);
    return $this->_get_fields($table);
  }

  protected function _error() {}
  protected function _connect() {}
  protected function _disconnect() {}
  protected function _query($query) {}
  protected function _fetch_array($dbq) {}
  protected function _fetch_object($dbq) {}
  protected function _count($dbq) {}
  protected function _truncate($table) {}
  protected function _show_create_table($table, $dst_table) {}
  protected function _drop_table($table) {}
  protected function _get_tables() {}
  protected function _get_fields($table) {}
}

