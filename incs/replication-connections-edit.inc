<?php
/**
 * @file
 * A form to add or edit a connection.
 */

/**
 * Form builder for the add or edit connection form.
 *
 * @param $cid
 *   The connections CID to edit, NULL to create a new connection.
 *
 * @see replication_manager_connections_edit_validate()
 * @see replication_manager_connections_edit_submit()
 * @ingroup forms
 */
function replication_manager_connections_edit(&$form_state, $cid = NULL) {
  $form = array();
  $connection = NULL;
  if (is_numeric($cid)) {
    $connection = replication_manager_load_connection($cid, TRUE);
    $form["#connection"] = $connection;
  }

  $form["general"] = array(
    "#type" => "fieldset",
    "#title" => t("General connection details"),
    "#collapsible" => TRUE,
    "connection_name" => array(
      "#type" => "textfield",
      "#title" => t("Name"),
      "#description" => t("The unique name of the connection."),
      "#default_value" => !empty($connection) ? $connection->connection_name : NULL,
      "#maxlength" => 255,
      "#required" => TRUE
    ),
    "connection_description" => array(
      "#type" => "textfield",
      "#title" => t("Description"),
      "#description" => t("A description of the connection."),
      "#default_value" => !empty($connection) ? $connection->connection_description : NULL,
      "#maxlength" => 255
    )
  );

  $dbtypes = replication_manager_dbtypes();
  $_dbtypes = array("" => t("Select a connection type"));
  foreach ($dbtypes as $key => $dbtype) $_dbtypes[$key] = $dbtype["name"];
  $maskpassword = variable_get("replication_manager_maskpasswords", TRUE);

  $form["connection"] = array(
    "#type" => "fieldset",
    "#title" => t("Connection details"),
    "#collapsible" => TRUE,
    "#collapsed" => !empty($connection),
    "connection_dbtype" => array(
      "#type" => "select",
      "#title" => t("Database type"),
      "#description" => t("Database connection type."),
      "#options" => $_dbtypes,
      "#default_value" => !empty($connection) ? $connection->dbtype : NULL,
      "#required" => TRUE
    ),
    "connection_username" => array(
      "#type" => "textfield",
      "#title" => t("Username"),
      "#description" => t("The username needed to connect to the database."),
      "#default_value" => !empty($connection) ? $connection->username : NULL,
      "#maxlength" => 255,
      "#required" => TRUE
    ),
    "connection_changepassword" => empty($connection) ? array("#value" => "") : array(
      "#type" => "checkbox",
      "#title" => t("Change password"),
      "#description" => t("Check this to change the password.")
    ),
    "connection_password" => array(
      "#type" => $maskpassword ? "password" : "textfield",
      "#title" => t("Password"),
      "#description" => t("The password needed to connect to the database."),
      "#default_value" => !empty($connection) ? $connection->password : NULL,
      "#maxlength" => 255
    ),
    "connection_host" => array(
      "#type" => "textfield",
      "#title" => t("Host"),
      "#description" => t("The host of the database server."),
      "#default_value" => !empty($connection) ? $connection->host : NULL,
      "#maxlength" => 255,
      "#required" => TRUE
    ),
    "connection_port" => array(
      "#type" => "textfield",
      "#title" => t("Port"),
      "#description" => t("The port of the database server. Leave blank to use the default <b>3306</b>."),
      "#default_value" => !empty($connection) ? $connection->port : NULL,
      "#maxlength" => 10
    ),
    "database_name" => array(
      "#type" => "textfield",
      "#title" => t("Database name"),
      "#description" => t("The name of the database to connect to."),
      "#default_value" => !empty($connection) ? $connection->database_name : NULL,
      "#maxlength" => 255,
      "#required" => TRUE
    ),
    "table_prefix" => array(
      "#type" => "textfield",
      "#title" => t("Table prefix"),
      "#description" => t("The prefix of the tables, eg. <b>drupal_</b>."),
      "#default_value" => !empty($connection) ? $connection->table_prefix : NULL,
      "#maxlength" => 255
    )
  );

  $form["submit"] = array(
    "#type" => "submit",
    "#value" => empty($cid) ? t("Create connection") : t("Save connection")
  );
  return $form;
}

/**
 * Form validation handler for replication_manager_connections_edit().
 *
 * @see replication_manager_connections_edit()
 * @see replication_manager_connections_edit_submit()
 */
function replication_manager_connections_edit_validate($form, &$form_state) {
  $connection = $form["#connection"];

  if (empty($connection) || $connection->connection_name !== $form_state["values"]["connection_name"]) {
    $dbr = db_fetch_object(db_query("SELECT * FROM {replication_connections} WHERE connection_name = '%s'", $form_state["values"]["connection_name"]));
    if (!empty($dbr)) {
      form_set_error("connection_name", t("A connection with this name already exists!"));
    }
  }

  if (!empty($form_state["values"]["connection_port"]) && !is_numeric($form_state["values"]["connection_port"]))
    form_set_error("connection_port", t("The connection port must be numeric!"));
}

/**
 * Form submission handler for replication_manager_connections_edit().
 *
 * @see replication_manager_connections_edit()
 * @see replication_manager_connections_edit_validate()
 */
function replication_manager_connections_edit_submit($form, &$form_state) {
  $connection = $form["#connection"];
  if (empty($connection)) $connection = new stdClass();

  $connection->connection_name = $form_state["values"]["connection_name"];
  $connection->connection_description = $form_state["values"]["connection_description"];
  $connection->dbtype = $form_state["values"]["connection_dbtype"];
  $connection->username = $form_state["values"]["connection_username"];

  if (empty($connection->cid) || $form_state["values"]["connection_changepassword"] == TRUE) {
    drupal_set_message(t("The password for connection !name has been changed.", array("!name" => $connection->connection_name)));
    $connection->password = replication_manager_crypt($form_state["values"]["connection_password"], REPLICATION_MANAGER_CONNECTION_CRYPTKEY . $connection->connection_name);
  }
  else {
    unset($connection->password);
  }

  $connection->host = $form_state["values"]["connection_host"];
  $connection->database_name = $form_state["values"]["database_name"];
  $connection->table_prefix = $form_state["values"]["table_prefix"];
  $connection->port = empty($form_state["values"]["connection_port"]) ? 3306 : $form_state["values"]["connection_port"];

  if (replication_manager_save_connection($connection) === FALSE) {
    drupal_set_message(t("There was a problem saving this connection!"), "error");
  }
  else {
    drupal_set_message(t("The connection <b>!name</b> has been !saved", array("!name" => $connection->connection_name, "!saved" => empty($form["#connection"]) ? t("Created") : t("Saved"))));
    $form_state["redirect"] = "admin/build/replication/connections/list";
  }
}

