<?php
/**
 * @file
 * A form to provide a job test..
 */

/**
 * Form builder for the job check form.
 *
 * @param $cid
 *   The jobs JID to check.
 *
 * @ingroup forms
 */
function replication_manager_jobs_check(&$form_state, $jid = NULL) {
  $job = NULL;
  if (is_numeric($jid)) $job = replication_manager_load_job($jid, TRUE);

  $form = array();
  if (empty($job)) {
    $form["info"] = array(
      "#prefix" => "<p>",
      "#value" => t("The provided job id <b>!jid</b> is invalid or failed to load a valid job.", array("!jid" => $jid)),
      "#suffix" => "</p>"
    );
  }

  $job_status = replication_manager_check_job($job);
  $_job_status = $job_status ? REPLICATION_MANAGER_JOB_STATUS_OK : REPLICATION_MANAGER_JOB_STATUS_ERROR;
  $job_type = replication_manager_jobtypes($job->job_type);

  $form["job"] = array(
    "#type" => "fieldset",
    "#title" => t("Job details"),
    "#collapsible" => TRUE,
    "job_name" => array(
      "#prefix" => "<div>",
      "#value" => t("<b>Name: </b>!name", array("!name" => $job->job_name)),
      "#suffix" => "</div>"
    ),
    "job_description" => array(
      "#prefix" => "<div>",
      "#value" => t("<b>Description: </b>!description", array("!description" => $job->job_description)),
      "#suffix" => "</div>"
    ),
    "job_type" => array(
      "#prefix" => "<div>",
      "#value" => t("<b>Job type: </b>!type", array("!type" => !empty($job_type) ? $job_type["name"] : t("Unknown job type"))),
      "#suffix" => "</div>"
    ),
    "job_status" => array(
      "#prefix" => "<div class=\"job-status job-status-{$_job_status}\">",
      "#value" => t("<b>Status: </b>!status<br />!error", array("!status" => ($job_status ? "<span style=\"color:green;\">" . t("successful") . "</span>" : "<span style=\"color:red;\">" . t("failed") . "</span>"), "!error" => $job->status_response)),
      "#suffix" => "</div>"
    )
  );

  $form["tasks"] = array(
    "#type" => "fieldset",
    "#title" => t("Tasks"),
    "#collapsible" => TRUE,
    "#collapsed" => TRUE
  );

  if (!$job_type["tasks"]) {
    $form["tasks"]["#value"] = t("This job type does not use tasks.");
  }
  else {
    /** task information. */
  }

  return confirm_form($form,
    "",
    "admin/build/replication/jobs/list",
    "",
    t("Check again"),
    t("Back to jobs list"),
    "replication_manager_jobs_check"
  );
}

