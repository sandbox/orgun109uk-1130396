<?php
/*******************************************************************************
 * @file
 * This file contains all the utility functions for jobs.
 */

/*******************************************************************************
 * Jobs.
 */
function replication_manager_check_job(&$job) {
  if (empty($job)) return FALSE;
  if (is_numeric($job)) $job = replication_load_job($job, TRUE);
  if (!is_object($job)) return FALSE;
  $job->status = REPLICATION_MANAGER_JOB_STATUS_ERROR;

  $job_type = replication_manager_jobtypes($job->job_type);
  if (empty($job_type)) {
    $job->status_response = t("Failed to retrieve the job type.");
    return FALSE;
  }
  elseif (!_replication_manager_jobtypes($job_type["module"], "check", $job, $job->status_response)) {
    return FALSE;
  }

  /** Check tasks. */
  if ($job_type["tasks"]) {
    if ($job->tasks == NULL) $job->tasks = replication_manager_load_task(NULL, $job->jid);
    if ($job->tasks == NULL) {
      $job->status_response = t("Either tasks have not been given to this job, or failed to retrieve any tasks.");
      return FALSE;
    }

    /** Check tasks... */
    foreach ($job->tasks as $tid => $task) {
      // @todo check tasks.
    }
  }
  return TRUE;
}

function replication_manager_load_job($jid = NULL, $inc_tasks = FALSE, $check_status = FALSE) {
  if (is_numeric($jid)) {
    $dbq = db_query("SELECT * FROM {replication_jobs} WHERE jid = %d", $jid);
  }
  else {
    $dbq = db_query("SELECT * FROM {replication_jobs}");
  }

  $jobs = array();
  while ($dbr = db_fetch_object($dbq)) {
    if ($inc_tasks) $dbr->tasks = replication_manager_load_task(NULL, $dbr->jid);
    if (is_string($dbr->job_settings)) $dbr->job_settings = unserialize($dbr->job_settings);
    if ($check_status) replication_manager_check_job($dbr);
    $jobs[$dbr->jid] = $dbr;
  }

  if (is_numeric($jid)) return $jobs[$jid];
  return $jobs;
}

function replication_manager_save_job(&$job) {
  if (is_array($job)) {
    $r = array();
    foreach ($job as &$j)
      $r[] = replication_manager_save_job($j);
    return $r;
  }

  if (empty($job->jid)) {
    global $user;
    $job->created = time();
    $job->created_uid = $user->uid;
  }

  $job->modified = time();
  $job->modified_uid = $user->uid;
  return drupal_write_record("replication_jobs", $job, !empty($job->jid) ? "jid" : array());
}

function replication_manager_delete_job(&$job) {
  if (is_array($job)) {
    $r = array();
    foreach ($job as &$j)
      $r[] = replication_manager_delete_job(&$j);
    return $r;
  }

  return db_query("DELETE FROM {replication_jobs} WHERE jid = %d", $job->jid);
}

function replication_manager_run_job(&$job, $from = REPLICATION_MANAGER_JOB_RUNMODE_EXEC, $action_name = NULL) {
  if (is_numeric($job)) $job = replication_manager_load_job($job);
  if ($job == NULL || empty($job->jid)) return NULL;

  $context = new stdClass();
  $context->trigger = ($from == REPLICATION_MANAGER_JOB_RUNMODE_TRIGGER);
  $context->cron = ($from == REPLICATION_MANAGER_JOB_RUNMODE_CRON);
  $context->action = ($from == REPLICATION_MANAGER_JOB_RUNMODE_ACTION);
  $context->action_name = $action_name;
  $context->display_message = TRUE;
  $context->redirect = "";

  _replication_manager_process("preprocess", $job, $context);
  $job_type = replication_manager_jobtypes($job->job_type);
  if (empty($job_type)) {
    $context->status_response = t("Attempted to trigger a replication job with an invalid job type (!jid).", array("!jid" => $jid));
    $context->status_reason = "invalid-job-type";
    $context->success = FALSE;
    _replication_manager_process("fail", $job, $context);

    if ($context->display_message) watchdog("Replication", $context->status_response, array(), WATCHDOG_ERROR);
    return $context;
  }

  if (empty($context->job_type))
    $context->job_type = $job_type;
  else
    $job_type = $context->job_type;

  if (empty($job_type["triggable"])) {
    $context->status_response = t("Attempted to trigger a replication job that does not permit it (!jid).", array("!jid" => $jid));
    $context->status_reason = REPLICATION_MANAGER_JOB_RESULT_NOTTRIGGABLE;
    $context->success = FALSE;
    _replication_manager_process("fail", $job, $context);

    if ($context->display_message) watchdog("Replication", $context->status_response, array(), WATCHDOG_ERROR);
    return $context;
  }

  _replication_manager_process("precheck", $job, $context);
  if (!replication_manager_check_job($job)) {
    $context->status_response = t("Attempted to trigger a replication job that failed its check (!jid). !error", array("!jid" => $jid, "!error" => $job->status_response));
    $context->status_reason = REPLICATION_MANAGER_JOB_RESULT_FAILEDCHECK;
    $context->success = FALSE;
    _replication_manager_process("fail", $job, $context);

    if ($context->display_message) watchdog("Replication", $context->status_response, array(), WATCHDOG_ERROR);
    return $context;
  }

  _replication_manager_process("prerun", $job, $context);

  $success = _replication_manager_jobtypes($job_type["module"], "run", $job, $context);
  if (!$success) {
    $context->status_response = t("The replication job !name has failed.<br />Response: !response", array("!name" => $job->job_name, "!response" => $context->status_response));
    $context->status_reason = REPLICATION_MANAGER_JOB_RESULT_FAILEDRUN;
  }
  else {
    $context->status_reason = REPLICATION_MANAGER_JOB_RESULT_SUCCESS;
  }
  $context->success = $success;
  _replication_manager_process("postrun", $job, $context);

  $job->runs++;
  if ($success)
    $job->successes++;
  else
    $job->fails++;

  global $user;
  db_query("UPDATE {replication_jobs} SET runs = %d, successes = %d, fails = %d, last_run = '%s', last_run_uid = %d WHERE jid = %d", $job->runs, $job->successes, $job->fails, "" . time(), $user->uid, $job->jid);
  return $context;
}

