<?php
/**
 * @file
 * A form to add or edit a job.
 */

/**
 * Form builder for the add or edit job form.
 *
 * @param $jid
 *   The jobs JID to edit, NULL to create a new job.
 *
 * @see replication_manager_jobs_edit_validate()
 * @see replication_manager_jobs_edit_submit()
 * @ingroup forms
 */
function replication_manager_jobs_edit(&$form_state, $jid = NULL) {
  $form = array();
  $job = NULL;
  if (is_numeric($jid)) {
    $job = replication_manager_load_job($jid);
    $form["#job"] = $job;
  }

  $form["general"] = array(
    "#type" => "fieldset",
    "#title" => t("General job details"),
    "#collapsible" => TRUE,
    "job_name" => array(
      "#type" => "textfield",
      "#title" => t("Name"),
      "#description" => t("The unique name of the job."),
      "#default_value" => !empty($job) ? $job->job_name : NULL,
      "#maxlength" => 255,
      "#required" => TRUE
    ),
    "job_description" => array(
      "#type" => "textfield",
      "#title" => t("Description"),
      "#description" => t("A description of the job."),
      "#default_value" => !empty($job) ? $job->job_description : NULL,
      "#maxlength" => 255
    )
  );

  $jobtype = NULL;
  if (!empty($job->job_type)) {
    $jobtype = replication_manager_jobtypes($job->job_type);
  }

  if (empty($jobtype)) {
    $jobtypes = replication_manager_jobtypes();
    $_jobtypes = array("" => t("Select a job type"));
    foreach ($jobtypes as $key => $_jobtype) $_jobtypes[$key] = $_jobtype["name"];

    $form["job"] = array(
      "#type" => "fieldset",
      "#title" => t("Job details"),
      "#description" => !empty($job->job_type) ? t("The job type that was selected for this job is no longer available.") : NULL,
      "#collapsible" => TRUE,
      "job_type" => array(
        "#type" => "select",
        "#title" => t("Job type"),
        "#description" => t("Select a job type."),
        "#options" => $_jobtypes,
        "#default_value" => !empty($job) ? $job->job_type : NULL,
        "#required" => TRUE
      )
    );
  }
  else {
    $form["#jobtype"] = $jobtype;
    $form["jobsettings"] = array(
      "#type" => "fieldset",
      "#title" => t("Job settings"),
      "#collapsible" => TRUE
    );

    $job_settings_form = _replication_manager_jobtypes($jobtype["module"], "edit", $job);
    if (is_array($job_settings_form) && !empty($job_settings_form)) {
      $form["jobsettings"] = array_merge($form["jobsettings"], $job_settings_form);
    }
  }

  $form["submit"] = array(
    "#type" => "submit",
    "#value" => empty($jid) ? t("Create job") : t("Save job")
  );
  return $form;
}

/**
 * Form validation handler for replication_manager_jobs_edit().
 *
 * @see replication_manager_jobs_edit()
 * @see replication_manager_jobs_edit_submit()
 */
function replication_manager_jobs_edit_validate($form, &$form_state) {
  $job = $form["#job"];
  $jobtype = $form["#jobtype"];

  if (empty($job) || $job->job_name !== $form_state["values"]["job_name"]) {
    $dbr = db_fetch_object(db_query("SELECT * FROM {replication_jobs} WHERE job_name = '%s'", $form_state["values"]["job_name"]));
    if (!empty($dbr)) {
      form_set_error("job_name", t("A job with this name already exists!"));
    }
  }

  if (!empty($job) && !empty($jobtype)) {
    _replication_manager_jobtypes($jobtype["module"], "edit_validate", $job, $form_state);
  }
}

/**
 * Form submission handler for replication_manager_jobs_edit().
 *
 * @see replication_manager_jobs_edit()
 * @see replication_manager_jobs_edit_validate()
 */
function replication_manager_jobs_edit_submit($form, &$form_state) {
  $job = $form["#job"];
  $jobtype = $form["#jobtype"];
  if (empty($job)) $job = new stdClass();

  $job->job_name = $form_state["values"]["job_name"];
  $job->job_description = $form_state["values"]["job_description"];
  if (empty($jobtype)) {
    $job->job_type = $form_state["values"]["job_type"];
  }

  if (!empty($job) && !empty($jobtype)) {
    $job->job_settings = _replication_manager_jobtypes($jobtype["module"], "edit_submit", $job, $form_state);
  }

  if (replication_manager_save_job($job) === FALSE) {
    drupal_set_message(t("There was a problem saving this job!"), "error");
  }
  else {
    drupal_set_message(t("The job <b>!name</b> has been !saved", array("!name" => $job->job_name, "!saved" => empty($form["#job"]) ? t("Created") : t("Saved"))));
    $form_state["redirect"] = "admin/build/replication/jobs/list";
  }
}

