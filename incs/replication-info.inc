<?php
/**
 * @file
 * Displays information regarding the replication and all available dbtypes,
 * connections, jobs, tasks and basic stats.
 */

/**
 * Form builder for the connections list form.
 *
 * @ingroup forms
 */
function replication_manager_info(&$form_state) {
  $maskpassword = variable_get("replication_manager_maskpasswords", TRUE);

  $form = array();
  $connections = replication_manager_load_connection();
  $dbtypes = replication_manager_dbtypes();

  /** Basic statistics. */
  $rows = array();
  $rows[] = array(
    "data" => array(
      array("data" => t("Number of connections"), "class" => "replication-info-stats-title"),
      array("data" => count($connections), "class" => "replication-info-stats-value"),
    ),
    "class" => "replication-info-stats-connectionscount"
  );

  $rows[] = array(
    "data" => array(
      array("data" => t("Number of database types"), "class" => "replication-info-stats-title"),
      array("data" => count($dbtypes), "class" => "replication-info-stats-value"),
    ),
    "class" => "replication-info-stats-dbtypescount"
  );

  $rows[] = array(
    "data" => array(
      array("data" => t("Successfull jobs"), "class" => "replication-info-stats-title"),
      array("data" => 0, "class" => "replication-info-stats-value"),
    ),
    "class" => "replication-info-stats-successfulljobs"
  );

  $form["status"] = array(
    "#type" => "fieldset",
    "#title" => t("Replication statistics"),
    "#collapsible" => TRUE,
    "#collapsed" => TRUE,
    "#value" => theme("table", NULL, $rows)
  );

  /** Connections details. */
  $header = array(
    t("Connection name"),
    t("Connection")
  );

  $rows = array();
  if (!empty($connections)) {
    foreach ($connections as $key => $connection) {
      if (!$maskpassword) {
        replication_manager_unmask_connection($connection);
        $password = $connection->password;
      }
      else {
        $password = replication_manager_maskpassword($connection->password);
      }

      $rows[] = array(
        "data" => array(
          array("data" => $connection->connection_name, "class" => "replication-info-connection-name"),
          array("data" => "{$connection->dbtype}://{$connection->username}:{$password}@{$connection->host}:{$connection->port}/{$connection->database_name}", "class" => "replication-info-connection-connection")
        )
      );
    }
  }
  else {
    $rows[] = array(
      "data" => array(
        array("data" => t("There are no database types installed. Enable at least 1 from the !module list", array("!module" => l(t("modules"), "admin/build/modules"))), "colspan" => 2)
      )
    );
  }

  $form["connections"] = array(
    "#type" => "fieldset",
    "#title" => t("Available connections (!count)", array("!count" => count($connections))),
    "#collapsible" => TRUE,
    "#collapsed" => TRUE,
    "#value" => theme("table", $header, $rows)
  );

  /** Database types details. */
  $header = array(
    t("Database type name"),
    t("No. of connections")
  );

  $rows = array();
  if (!empty($dbtypes)) {
    foreach ($dbtypes as $key => $dbtype) {
      $rows[] = array(
        "data" => array(
          array("data" => $dbtype["name"], "class" => "replication-info-dbtype-name"),
          array("data" => db_result(db_query("SELECT COUNT(cid) FROM {replication_connections} WHERE dbtype = '%s'", $key)), "class" => "replication-info-dbtype-count")
        )
      );
    }
  }
  else {
    $rows[] = array(
      "data" => array(
        array("data" => t("There are no database types installed. Enable at least 1 from the !module list", array("!module" => l(t("modules"), "admin/build/modules"))), "colspan" => 2)
      )
    );
  }

  $form["dbtypes"] = array(
    "#type" => "fieldset",
    "#title" => t("Available database types (!count)", array("!count" => count($dbtypes))),
    "#collapsible" => TRUE,
    "#collapsed" => TRUE,
    "#value" => theme("table", $header, $rows)
  );

  return $form;
}

