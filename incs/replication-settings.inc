<?php
/**
 * @file
 * A form to administer the replication module settings.
 */

/**
 * Form builder for the replication module settings.
 *
 * @ingroup forms
 */
function replication_manager_settings(&$form_state) {
  $form = array();

  /** General Settings. */
  $form["general"] = array(
    "#type" => "fieldset",
    "#title" => t("General settings"),
    "#collapsible" => TRUE,
    "#collapsed" => TRUE,
    "replication_manager_maskpasswords" => array(
      "#type" => "checkbox",
      "#title" => t("Mask passwords"),
      "#description" => t("Mask all passwords in the edit/view forms. This does not effect the encrypted password in the table."),
      "#default_value" => variable_get("replication_manager_maskpasswords", TRUE)
    ),
    "replication_manager_connectionslist_autostatus" => array(
      "#type" => "checkbox",
      "#title" => t("Connections list status check"),
      "#description" => t("Check all connections when loading the connections list. Note: This will increase the pages load time."),
      "#default_value" => variable_get("replication_manager_connectionslist_autostatus", FALSE)
    ),
    "replication_manager_jobslist_autostatus" => array(
      "#type" => "checkbox",
      "#title" => t("jobs list status check"),
      "#description" => t("Check all jobs when loading the jobs list. Note: This will increase the pages load time."),
      "#default_value" => variable_get("replication_manager_jobslist_autostatus", FALSE)
    )
  );

  /** Notification Settings. */
  $form["notifications"] = array(
    "#type" => "fieldset",
    "#title" => t("Notifications"),
    "#description" => t("Notification settings."),
    "#collapsible" => TRUE,
    "#collapsed" => TRUE,
    "replication_manager_notifications_emailfailure" => array(
      "#type" => "checkbox",
      "#title" => t("Email on failure"),
      "#description" => t("Check to send an email if there was a problem or failure."),
      "#default_value" => variable_get("replication_manager_notifications_emailfailure", FALSE)
    ),
    "replication_manager_notifications_emailfailureaddress" => array(
      "#type" => "textfield",
      "#title" => t("Email failure to"),
      "#description" => t("Enter an email address to send the failure to, seperate multiple accounts with a <b>,</b> (comma). Leave empty to use the site email address."),
      "#default_value" => variable_get("replication_manager_notifications_emailfailureaddress", "")
    )
  );

  return system_settings_form($form);
}

