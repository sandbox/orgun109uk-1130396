<?php
/*******************************************************************************
 * @file
 * This file contains helper functions.
 */

/**
 * Returns an encrypted/decrypted string.
 *
 * @param $password
 *   The string that you want encrypted or decrypted. If this string is
 *   already encrypted, the returned string will be its decrypted equivalent.
 * @param $key
 *   The encryption/decryption key.
 * @return
 *   Returns either the encrypted or decrypted version of $password.
 */
function replication_manager_crypt($password, $key) {
  if ($key == "") return $password;

  $_kl = drupal_strlen($key);
  $key = str_replace(chr(32), "", $key);
  if ($_kl < 8) {
    drupal_set_message(t("Failed to generate password crypt."));
    return $password;
  }

  $kl = $_kl < 32 ? drupal_strlen($key) : 32;
  $k = array();
  for ($i = 0; $i < $kl; $i++) $k[$i] = ord($key{$i})&0x1F;

  $j = 0;
  $_pl = drupal_strlen($password);
  for ($i = 0; $i < $_pl; $i++) {
    $e = ord($password{$i});
    $password{$i} = $e&0xE0 ? chr($e^$k[$j]) : chr($e);
    $j++;
    $j = $j == $kl ? 0 : $j;
  }

  return $password;
}

/**
 * Replaces each character of the given password with a mask.
 *
 * @param $password
 *   The password/string to mask.
 * @param $mask
 *   The mask string, by default '*' is used.
 *
 * @return
 *   Returns the masked password.
 */
function replication_manager_maskpassword($password, $mask = "*") {
  return str_pad("", drupal_strlen($password), $mask);
}

/**
 * Get a list of all available database types.
 *
 * @param $dbtype
 *   Use a database types name to return that database types info.
 *
 * @return
 *   Either returns a single database types info if $dbtype was given,
 *   otherwise an array of all database types.
 */
function replication_manager_dbtypes($dbtype = NULL) {
  global $_replication_manager_dbtypes;
  if (empty($_replication_manager_dbtypes)) {
    $_replication_manager_dbtypes = _replication_manager_dbtypes();
  }

  if (!empty($dbtype)) return $_replication_manager_dbtypes[$dbtype];
  return $_replication_manager_dbtypes;
}

/**
 * Get a list of all available job types.
 *
 * @param $jobtype
 *   Use a job types name to return that job types info.
 *
 * @return
 *   Either returns a single job types info if $jobtype was given,
 *   otherwise an array of all job types.
 */
function replication_manager_jobtypes($jobtype = NULL) {
  global $_replication_manager_jobtypes;
  if (empty($_replication_manager_jobtypes)) {
    $_replication_manager_jobtypes = _replication_manager_jobtypes();
  }

  if (!empty($jobtype)) return $_replication_manager_jobtypes[$jobtype];
  return $_replication_manager_jobtypes;
}

