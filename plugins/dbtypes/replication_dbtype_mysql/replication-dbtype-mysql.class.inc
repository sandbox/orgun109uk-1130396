<?php
/**
 * @file
 * The MySQL database class, extended from ReplicationDBType.
 */

class ReplicationDBType_MySQL extends ReplicationDBType {
  protected function _error() {
    return mysql_error();
  }

  protected function _connect() {
    $connection = mysql_connect("{$this->settings->host}:{$this->settings->port}", $this->settings->username, $this->settings->password, TRUE);
    if (!mysql_select_db($this->settings->database_name, $connection)) {
        $this->status_response = t("Failed to select database !name. !error", array("!name" => $this->settings->database_name, "!error" => mysql_error()));
        return FALSE;
    }
    return $connection;
  }

  protected function _disconnect() {
    mysql_close($this->connection);
  }

  protected function _query($query) {
    return mysql_query($query, $this->connection);
  }

  protected function _fetch_array($dbq) {
    return mysql_fetch_array($dbq);
  }

  protected function _fetch_object($dbq) {
    return mysql_fetch_object($dbq);
  }

  protected function _count($dbq) {
    return mysql_num_rows($dbq);
  }

  protected function _truncate($table) {
    return $this->query("TRUNCATE TABLE {$table}");
  }

  protected function _show_create_table($table, $dst_table) {
    $dbq = $this->query("SHOW CREATE TABLE {$table}");
    if (!$dbq) return FALSE;
    $dbr = mysql_fetch_array($dbq);
    $query = str_replace("TABLE `{$table}`", "TABLE `{$dst_table}`", $dbr[1]);
    return $query;
  }

  protected function _drop_table($table) {
    return $this->query("DROP TABLE IF EXISTS {$table}");
  }

  protected function _get_tables() {
    return $this->query("SHOW TABLES LIKE '{$this->settings->table_prefix}%'");
  }

  protected function _get_fields($table) {
    $dbq = $this->query("SHOW COLUMNS FROM {$table}");
    if (!$dbq) {
      $this->status_response = t("Could not run query: !error", array("!error" => mysql_error()));
      return FALSE;
    }

    $fields = array();
    if (mysql_num_rows($dbq) > 0) {
      while ($dbr = mysql_fetch_array($dbq)) {
        $fields[$dbr[0]] = array(
          "name" => $dbr[0],
          "type" => $dbr[1],
          "null" => $dbr[2],
          "primary" => $dbr[3] == "PRI",
          "key" => $dbr[3],
          "default" => $dbr[4],
          "extra" => $dbr[5]
        );
      }
    }
    return $fields;
  }
}

