<?php
/**
 * @file
 * Provides a replication task type, to copy from one database to another.
 *
 * This module must contain the following hooks:
 *   - hook_replication_manager_jobtypes()
 */

/*******************************************************************************
 * Hooks
 */

/**
 * Implements hook_replication_manager_jobtypes().
 *
 * @param $op
 *   The jobtypes operation. ("info", "run")
 * @param $job
 *   This value changes depending on the operation..
 *     - "info" this is NULL.
 *     - "edit" this is a reference to the job object.
 *     - "edit_validate" this is a reference to the job object.
 *     - "edit_submit" this is a reference to the job object.
 *     - "check" this is a reference to the job object.
 *     - "run"  this is a reference to the job object.
 * @param $context
 *   This value changes depending on the operation..
 *     - "info" this is NULL.
 *     - "edit" this is NULL.
 *     - "edit_validate" the form_state object.
 *     - "edit_submit" the form_state object.
 *     - "check" this is a error response string.
 *     - "run"  this contains details on the context.
 *
 * @return
 *   Depending on the operation..
 *     - "info" An array containing the job type information.
 *     - "edit" form fields array.
 *     - "edit_validate" Nothing.
 *     - "edit_submit" An object containing the job settings.
 *     - "check"  TRUE or FALSE depending on if check was successfull.
 *     - "run"  TRUE or FALSE depending on if successfully run.
 *
 *   Job type array: Array(
 *     "name" => t("Job type") // The name of the database type.
 *     "description" => t("A job type") // Description of the job type.
       "cronable" => FALSE // Can this job type be run in a cron.
       "actionable" => TRUE // Can this job type be run via an action.
       "triggable" => FALSE // Can this job type be run via the run url.
       "tasks" => FALSE // Can this job type have tasks added to it.
 *   );
 */
function replication_manager_job_dbtodb_replication_manager_jobtypes($op = "info", &$job = NULL, &$context = NULL) {
  switch ($op) {
    case "info":
      return array(
        "dbtodb" => array(
          "name" => t("Database to Database"),
          "description" => t("Provides the ability to copy from one database to another database."),
          "cronable" => TRUE,
          "actionable" => TRUE,
          "triggable" => TRUE,
          "tasks" => FALSE
        )
      );
    case "edit":
      $connections = replication_manager_load_connection();
      $_connections = array("" => t("Select a connection."));
      foreach ($connections as $connection) $_connections[$connection->cid] = "{$connection->connection_name} ({$connection->dbtype})";

      return array(
        "job_dbtodb_sourceconnection" => array(
          "#type" => "select",
          "#title" => t("Source connection"),
          "#options" => $_connections,
          "#default_value" => !empty($job->job_settings->source_connection) ? $job->job_settings->source_connection : NULL,
          "#required" => TRUE
        ),
        "job_dbtodb_destconnection" => array(
          "#type" => "select",
          "#title" => t("Destination connection"),
          "#options" => $_connections,
          "#default_value" => !empty($job->job_settings->dest_connection) ? $job->job_settings->dest_connection : NULL,
          "#required" => TRUE
        ),
        "job_dbtodb_dropandcreatetables" => array(
          "#type" => "checkbox",
          "#title" => t("Drop and create tables"),
          "#description" => t("Drop the existing tables and create them in the destination table."),
          "#default_value" => !empty($job->job_settings->drop_create) ? $job->job_settings->drop_create : NULL
        ),
        "job_dbtodb_replicationtype" => array(
          "#type" => "select",
          "#title" => t("Replication type"),
          "#description" => t("Select the replication type."),
          "#options" => array(
            "" => t("Select a replication type"),
            "di" => t("Delete and insert (truncates table)"),
            "i" => t("Insert"),
            "ii" => t("Insert ignore (does not update existing data)"),
            "r" => t("Replace (overwrites existing data)")
          ),
          "#default_value" => !empty($job->job_settings->replication_type) ? $job->job_settings->replication_type : NULL,
          "#required" => TRUE
        ),
        "job_dbtodb_listtype" => array(
          "#type" => "select",
          "#title" => t("Inclusion or exclusion list type"),
          "#description" => t("Is the list below an inclusion list or an exclusion list?"),
          "#options" => array(
            "inc" => t("An inclusion list"),
            "exc" => t("An exclusion list")
          ),
          "#default_value" => !empty($job->job_settings->table_list_type) ? $job->job_settings->table_list_type : "inc"
        ),
        "job_dbtodb_list" => array(
          "#type" => "textarea",
          "#title" => t("Inclusion or exclusion list"),
          "#description" => t("A list of table names to include/exclude. Each table name to be on a seperate line and must not contain the prefix, and you can use <b>*</b> as a wildcard."),
          "#default_value" => !empty($job->job_settings->table_list) ? $job->job_settings->table_list : NULL
        )
      );
    case "edit_validate":
      if ($context["values"]["job_dbtodb_sourceconnection"] == $context["values"]["job_dbtodb_destconnection"])
        form_set_error("job_dbtodb_destconnection", t("The source and destination connections must not be the same!"));
      break;
    case "edit_submit":
      $r = new stdClass();
      $r->source_connection = $context["values"]["job_dbtodb_sourceconnection"];
      $r->dest_connection = $context["values"]["job_dbtodb_destconnection"];
      $r->drop_create = $context["values"]["job_dbtodb_dropandcreatetables"];
      $r->replication_type = $context["values"]["job_dbtodb_replicationtype"];
      $r->table_list_type = $context["values"]["job_dbtodb_listtype"];
      $r->table_list = $context["values"]["job_dbtodb_list"];
      return $r;
    case "check":
      $job->status = REPLICATION_MANAGER_JOB_STATUS_ERROR;
      $js = (array) $job->job_settings;
      if (empty($js)) {
        $context = t("The job has not yet been configured.");
        return FALSE;
      }
      elseif (empty($job->job_settings->source_connection)) {
        $context = t("The source connection has not been set.");
        return FALSE;
      }
      elseif (empty($job->job_settings->dest_connection)) {
        $context = t("The destination connection has not been set.");
        return FALSE;
      }
      elseif (empty($job->job_settings->replication_type)) {
        $context = t("The replication type has not been set.");
        return FALSE;
      }

      $source = $job->job_settings->source_connection;
      $dest = $job->job_settings->dest_connection;
      if (!replication_manager_check_connection($source)) {
        $context = t("The source connection failed its check. !error", array("!error" => $source->status_response));
        return FALSE;
      }
      elseif (!replication_manager_check_connection($dest)) {
        $context = t("The destination connection failed its check. !error", array("!error" => $dest->status_response));
        return FALSE;
      }

      $job->status = REPLICATION_MANAGER_JOB_STATUS_OK;
      return TRUE;
    case "run":
      $src_connection = replication_manager_load_connection($job->job_settings->source_connection, TRUE);
      $src_dbo = replication_manager_dbtype_object($src_connection);
      $dst_connection = replication_manager_load_connection($job->job_settings->dest_connection, TRUE);
      $dst_dbo = replication_manager_dbtype_object($dst_connection);

      if (empty($src_dbo)) {
        $context->status_response = t("Failed to connect to the source connection, !error", array("!error" => $src_connection->status_response));
        return FALSE;
      }
      elseif (empty($dst_dbo)) {
        $context->status_response = t("Failed to connect to the destination connection, !error", array("!error" => $dst_connection->status_response));
        return FALSE;
      }

      $src_tables = $src_dbo->get_tables(TRUE);
      if ($job->job_settings->drop_create) {
        $dst_dbo->drop_tables();
        $job->job_settings->replication_type = "i";
      }
      $dst_tables = $dst_dbo->get_tables(TRUE);

      // @todo inclusion/exclusion list

      foreach ($src_tables as $src_table_name => $src_table) {
        if (empty($src_connection->table_prefix))
          $dst_table = $dst_connection->table_prefix . $src_table_name;
        else
          $dst_table = str_replace($src_connection->table_prefix, $dst_connection->table_prefix, $src_table_name);

        if (empty($dst_tables[$dst_table])) {
          $query = $src_dbo->show_create_table($src_table_name, $dst_table);
          $dst_dbo->query($query);
        }

        if ($job->job_settings->replication_type == "di") $dst_dbo->truncate($dst_table);
        $dbq = $src_dbo->query("SELECT * FROM {$src_table_name}");

        while ($dbr = $src_dbo->fetch_object($dbq)) {
          $cols = array();
          $vals = array();
          foreach ($dbr as $k => $v) {
            $cols[] = $k;
            if (strpos($src_tables[$src_table_name][$k]["type"], "varchar") !== FALSE ||
                strpos($src_tables[$src_table_name][$k]["type"], "text") !== FALSE ||
                strpos($src_tables[$src_table_name][$k]["type"], "blob") !== FALSE)
              $vals[] = "'" . str_replace("'", "\'", $v) . "'";
            else
              $vals[] = $v;
          }

          switch ($job->job_settings->replication_type) {
            case "di": // Delete and insert.
            case "i": // Insert.
              $q = "INSERT INTO {$dst_table} (!cols) VALUES (!vals)";
              $q = str_replace("!cols", implode(", ", $cols), $q);
              $q = str_replace("!vals", implode(", ", $vals), $q);
              break;
            case "ii": // Insert ignore.
              $q = "INSERT IGNORE INTO {$dst_table} SET ";
              foreach ($cols as $k => $v) {
                if ($k > 0) $q .= ", ";
                $q .= "{$v} = {$vals[$k]}";
              }
              break;
            case "r": // Replace.
              $q = "REPLACE INTO {$dst_table} SET ";
              foreach ($cols as $k => $v) {
                if ($k > 0) $q .= ", ";
                $q .= "{$v} = {$vals[$k]}";
              }
              break;
          }

          $dst_dbo->query($q);
          $context->status_response = $dst_dbo->get_status_response();
        }
      }
      return TRUE;
  }
}

